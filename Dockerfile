#Stage 1 - Install dependencies and build the app
FROM cirrusci/flutter:beta AS build-env

# Install flutter dependencies
RUN flutter config --enable-web

WORKDIR /app
COPY . /app
RUN flutter build web

FROM nginx
COPY --from=build-env /app/build/web /usr/share/nginx/html